Name:    orc
Version: 0.4.40
Release: 1
Summary: The Oil Run-time Compiler
License: BSD-2-Clause AND BSD-3-Clause
URL:     https://gstreamer.freedesktop.org/
Source0: https://gstreamer.freedesktop.org/src/orc/%{name}-%{version}.tar.xz

BuildRequires: gtk-doc
BuildRequires: meson >= 0.55.0

%description
Orc is the sucessor to Liboil - The Library of Optimized Inner Loops.
Orc is a library and set of tools for compiling and executing very
simple programs that operate on arrays of data.  The "language" is
a generic assembly language that represents many of the features
available in SIMD architectures, including saturated addition and
subtraction, and many arithmetic operations.

%package_help

%package devel
Summary: Development package for Orc
Requires: %{name} = %{version}-%{release}
Requires: %{name}-compiler = %{version}-%{release}

%description devel
Development package for Orc.

%package compiler
Summary: The Orc compiler
Requires: %{name} = %{version}-%{release}

%description compiler
The Orc compiler.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson -D default_library=shared
%meson_build

%install
%meson_install

%check
%meson_test

%files
%license COPYING
%doc README
%{_libdir}/liborc-*.so.*
%{_bindir}/orc-bugreport

%files devel
%{_libdir}/liborc-*.so
%{_libdir}/pkgconfig/orc-0.4.pc
%{_libdir}/pkgconfig/orc-test-0.4.pc
%{_includedir}/%{name}-0.4/
%doc examples/*.c

%files compiler
%{_bindir}/orcc

%files help
%doc %{_datadir}/gtk-doc/html/orc/

%changelog
* Sun Sep 15 2024 Funda Wang <fundawang@yeah.net> - 0.4.40-1
- update to 0.4.40

* Wed Aug 14 2024 Funda Wang <fundawang@yeah.net> - 0.4.39-1
- update to 0.4.39

* Tue Jul 30 2024 zhangxingrong <zhangxingrong@uniontech.cn> - 0.4.34-3
- orc: Fix warning because of a mismatched OrcExecutor function signature
- x86insn: Fix binutils warning when comparing with sized immediate operand
- orctarget: Fix default target selection not applying when retrieving it by name

* Mon Jul 29  2024 baiguo <baiguo@kylinos.cn> - 0.4.34-2
- Use vasprintf() if available for error messages and otherwise vsnprintf();orccompiler, orcparse: Use secure UCRT printing functions on Windows

* Wed Jul 12 2023 dillon chen <dillon.chen@gmail.com> - 0.4.34-1
- update to 0.4.34

* Fri Nov 18 2022 dillon chen <dillon.chen@gmail.com> - 0.4.33-1
- update to 0.4.33

* Tue Oct 25 2022 wangjiang <wangjiang37@h-partners.com> 0.4.32-2
- Rebuild for next release

* Thu Jan 28 2021 yuanxin <yuanxin24@huawei.com> - 0.4.32-1
-upgrade version to 0.4.32

* Fri Apr 24 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.4.31-1
- Upgrade version to 0.4.31

* Tue Feb 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.4.28-5
- Enable check on aarch64

* Mon Aug 12 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.4.28-4
- Package init
